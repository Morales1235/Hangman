#ifndef MAIN_H
#define MAIN_H

void MainLoop();
void printInfo();
void parseWord();
void draw_hangman();
void gameOver();

#endif // MAIN_H
