#include <iostream>
#include <cstring>
#include <stdio.h>
#include "main.h"

using namespace std;

char letter;
string  buffer = "_________________________________"; ///!param buffer contains guessed letters
string my_word;             ///!Given word for guess
int score = 0;
bool hit = false, play_more;

int main(int argc, char *argv[])
{
    printf("Put a word\n");
    scanf("%s", my_word.c_str());
    system("clear");
    printf("Your word is %d long\n", strlen(my_word.c_str()));
    MainLoop();
    gameOver();
    return 0;
}

void MainLoop()
{
    while (score < 10)
    {
        printInfo();
        parseWord();

        if (!hit)
            score++;
        hit = false;
        printf("You have missed %d times\n", score);

        if (play_more == false)
        {
            printf("You won!\n");
            break;
        }
        draw_hangman();
    }
}

void printInfo()
{
    printf("Put a letter\n");
    scanf(" %c", &letter);
    system("clear");
}

void parseWord()
{
    play_more = false;
    for (int i = 0; i < strlen(my_word.c_str()); i++)
    {
        if (letter == my_word[i])       //checks if given letters exist in given word
        {
            buffer[i] = my_word[i];     //if word has a given letter then add letter to buffer
            hit = true;
        }
        if (buffer[i] == my_word[i]) printf("%c", buffer[i]);
        else printf("_");
        if (buffer[i] == '_') play_more = true;
    }
    printf("\r\n");
}

void draw_hangman()        //!Function for "draw" graphical simulate of hangman
{
    switch (score) {
    case 1:
        printf("__\n");
        break;
    case 2:
        printf(" |\n |\n |\n |\n_|_\n");
        break;
    case 3:
        printf(" ___\n |\n |\n |\n |\n_|_\n");
        break;
    case 4:
        printf(" ___\n |  |\n |\n |\n |\n_|_\n");
        break;
    case 5:
        printf(" ___\n |  |\n |  o\n |\n |\n_|_\n");
        break;
    case 6:
        printf(" ___\n |  |\n |  o\n |  |\n |\n_|_\n");
        break;
    case 7:
        printf(" ___\n |  |\n |  o\n | /|\n |\n_|_\n");
        break;
    case 8:
        printf(" ___\n |  |\n |  o\n | /|\\ \n |\n_|_\n");
        break;
    case 9:
        printf(" ___\n |  |\n |  o\n | /|\\ \n | /\n_|_\n");
        break;
    case 10:
        printf(" ___\n |  |\n |  o\n | /|\\ \n | / \\\n_|_\n");
        break;
    default:
        break;
    }
}

void gameOver()
{
    if (play_more == true)
    {
        printf("You've been wrong %d times, you lost\n", score);
        printf("The word was: %s.\n", my_word.c_str());
    }
}
